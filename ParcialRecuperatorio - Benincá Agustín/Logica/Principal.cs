﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Eventing.Reader;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logica
{ 
    public class Principal
    {
        List<Empleado> ListaEmpleados = new List<Empleado>();
        List<Ticket> ListaTickets = new List<Ticket>();

        //CORRECCIÓN: ESTO TIENE QUE IR COMO UN MÉTODO ABSTRACTO DEL EMPLEADO QUE SE SOBREESCRIBA EN EL DESARROLLADOR Y EL LIDER
        public bool ValidacionDeTicket(string tipoTicket, int dni)
        {
            foreach (var item in ListaEmpleados)
            {
                if (dni == item.DNI)
                {
                    if (item is Desarrollador)
                    {
                        Desarrollador desarrollador = item as Desarrollador;

                        if (desarrollador.LiderACargo.AreaACargo == 1 && tipoTicket == "Acceso a base de datos")
                        {
                            return true;
                        }
                        return false;
                    }
                    else
                    {
                        if (tipoTicket == "Gestion de permisos" || tipoTicket == "Alta de Usuario")
                        {
                            return true;
                        }
                        return false;
                    }
                }
            }
            return false;
        }

        public void ActualizarTicket(int numeroTicket, int estadoTicket, string comentarioResolucion)
        {
            foreach (var item in ListaTickets)
            {
                if (numeroTicket == item.CodigoNumerico)
                {
                    item.Estado = estadoTicket;
                    item.ComentarioDeResolucion = comentarioResolucion;
                }
            }
        }
    }
}
