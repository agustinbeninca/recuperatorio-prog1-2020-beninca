﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logica
{
    public class Desarrollador:Empleado
    {
        public string NivelExperiencia { get; set; }
        public Lider LiderACargo { get; set; }

        public override string DevolverDetalleDeEmpleado()
        {
            return $"{base.DevolverDetalleDeEmpleado()}, -Su líder de proyecto es: {LiderACargo}.";
        }

        //CORRECCIÓN: ESTE MÉTODO ESTÁ DEMÁS, SI SE HUBIERA HECHO LA SOBREESCRITURA
        public Lider ObtenerLiderACargo()
        {
            return LiderACargo;
        }
    }
}
