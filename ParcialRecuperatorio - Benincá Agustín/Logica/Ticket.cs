﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logica
{
    public class Ticket
    {
        public int CodigoNumerico { get; set; }
        //CORRECCIÓN: SE DEBERIA USAR UN ENUMERADOR
        public string Tipo { get; set; } //1- Gestión de permisos, 2- Acceso a base de datos, 3- Alta de usuario
        public DateTime FechaCreacion { get; set; }
        public Empleado DatosEmpleado { get; set; }
        //CORRECCIÓN: SE DEBERIA USAR UN ENUMERADOR
        public int Estado { get; set; } //1- Abierto, 2- En proceso, 3- Resuelto
        public string ObservacionDelUsuario { get; set; }
        public string ComentarioDeResolucion { get; set; }

        int cont = 0;
        //CORRECCIÓN: ESTE DATO ESTABA COMO INFORMACIÓN EXTRA, NO HAÍA QUE RESOLVERLO Y TAMPOCO SE RESUELVE DE ESTA MANERA
        public void GenerarCodigoNumerico()
        {
            cont++;
            CodigoNumerico = cont;
        }

    }
}
