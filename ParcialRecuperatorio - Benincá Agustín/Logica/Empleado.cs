﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logica
{
    public class Empleado
    {
        public int DNI { get; set; }
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public string Area { get; set; }
        public DateTime FechaNacimiento { get; set; }
        public string Localidad { get; set; }

        public virtual string DevolverDetalleDeEmpleado()
        {
            return $"{Nombre}, {Apellido}, DNI: , {DNI}";
        }


    }
}
